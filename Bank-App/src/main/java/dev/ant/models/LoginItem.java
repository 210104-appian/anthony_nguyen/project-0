package dev.ant.models;

public abstract class LoginItem 
{
	protected int id;
	protected String name;
	protected String LoginName;
	protected String password;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getLoginName() {
		return LoginName;
	}

	public void setLoginName(String LoginName) {
		this.LoginName = LoginName;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}