package dev.ant.services;
import dev.ant.daos.BankDaos;
import dev.ant.daos.LoginItemDaoImpl;
import java.util.Scanner;
//Create a do while loop while the usertype is returned to not be null or empty
//Offer choice to login as customer or employee, or to sign up as a customer with a switch case
//Return a string that is either "customer" or employee AFTER the person is authenticated

public class LoginService 
{
	String loginType = "";
	String returnedID = "";
	String inputID = "";
	String inputPW = "";
	String inputName = "";
	String returnedID2 = "";
	String returnedPW2 = "";
	
	@SuppressWarnings("unused")
	public String authenticate() 
	{

		int compareChoice;
		Scanner userInput = new Scanner(System.in);
		do {
			
			System.out.println("******* B A N K **********");
			System.out.println("* A) Register            *");
			System.out.println("* B) Sign in as customer *");
			System.out.println("* C) Sign in as employee *");
			System.out.println("* X) Exit                *");
			System.out.println("**************************\n");
			System.out.println("What would you like to do today ? ");
			char choice = userInput.next().toUpperCase().charAt(0);
			switch(choice) 
			{
			case 'A': //Registration//
				loginType = "Registration.";
				System.out.print("You selected " + loginType + "\nEnter UserID : ");
				inputID = userInput.next();
				LoginItemDaoImpl register1 = new LoginItemDaoImpl();
				String returnedUserIdCount = register1.getUserIdCount(inputID);
				if (returnedUserIdCount.compareTo("0") == 0) {
						System.out.println("Ready to Register \n");
						System.out.print("\nEnter Full Name : ");
						inputName = userInput.next();
						System.out.print("\nEnter Password : ");
						inputPW = userInput.next();
						LoginItemDaoImpl register2 = new LoginItemDaoImpl();
						String returnedUserRecord = register2.createUser(inputID,inputName,inputPW);
					}
				else {
					System.out.println("Unable to register.  UserID Already Exists.  (" + returnedUserIdCount + ")\n\n");
					compareChoice=0;
					}
				break;
			case 'B': //Customer Login//
				loginType = "Customer Login.";
				System.out.print("You selected " + loginType + "\nEnter UserID : ");
				inputID = userInput.next();
				LoginItemDaoImpl auth1 = new LoginItemDaoImpl();
				String returnedID = auth1.getUserId(inputID);
//				System.out.println("You are login as: " + inputID);
				
				System.out.print("\nEnter Password : ");
				inputPW = userInput.next();
				LoginItemDaoImpl auth2 = new LoginItemDaoImpl();
				String returnedPW = auth2.getPWFromUserID(returnedID);
				
//				System.out.println("Your password returns : " + returnedPW.length());	
//				System.out.println(inputID + "/" + inputPW.length() + " " + returnedPW.length());
				
				if (inputPW.compareTo(returnedPW) == 0)  {
					System.out.println("\nSuccessful Login.\n");
					
					LoginItemDaoImpl auth3 = new LoginItemDaoImpl();
					String returnedName = auth3.getAccountName(returnedID);
					
					LoginItemDaoImpl auth4 = new LoginItemDaoImpl();
					String returnedBalance = auth4.getAccountBalance(returnedID);
					
					LoginItemDaoImpl auth44 = new LoginItemDaoImpl();
					String returnedStatus = auth44.getUserStatus(returnedID);
					
					if (returnedStatus.compareTo("ACTIVE") == 0) {
						TransactionService transactionservice = new TransactionService(returnedName,returnedID, returnedBalance);
						transactionservice.transaction(returnedID);
					}
					else
						System.out.println("\nAccount is not active.  Contact administrator.\n");
					}
				else
					System.out.println("\nIncorrect Login.");
				break;
			case 'C':
				loginType = "EMPLOYEE Login.";
				System.out.print("You selected " + loginType + "\nEnter Employee ID : ");
				inputID = userInput.next();
				LoginItemDaoImpl auth21 = new LoginItemDaoImpl();
				String returnedID2 = auth21.getEmpId(inputID);
				System.out.print("\nEnter Password : ");
				inputPW = userInput.next();
				LoginItemDaoImpl auth22 = new LoginItemDaoImpl();
				String returnedPW2 = auth22.getPWFromEmpID(returnedID2);
				if (inputPW.compareTo(returnedPW2) == 0)  {
					System.out.println("\nSuccessful Login.\n");
					LoginItemDaoImpl auth23 = new LoginItemDaoImpl();
					String returnedName = auth23.getEmpName(returnedID2);
					EmployeeService employeeservice = new EmployeeService(returnedName,returnedID2);
				    employeeservice.administration(returnedID2);
					}
				break;
			case 'X':
				loginType = "Bye";
				break;
			default:
				System.out.println("Invalid option. Please try again." + choice);
				break;
			}
//			userInput.close();
//			System.out.println("Exit " + loginType);
			compareChoice = Character.compare(choice, 'X');
//			System.out.println(choice + "Exit " + compareChoice);
		}
//		Not leaving the while loop. Test the condition.
		while(compareChoice != 0);
//		
		return loginType;
//		userInput.close();
	}
}
