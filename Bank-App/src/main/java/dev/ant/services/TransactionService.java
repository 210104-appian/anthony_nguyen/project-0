package dev.ant.services;
import dev.ant.daos.BankDaos;
import java.util.Scanner;
import dev.ant.daos.LoginItemDaoImpl;
public class TransactionService
{
//Instantiating multiple attributes for a user's account	
//	TransactionRecord myFile1 = new TransactionRecord();
	double balance;
	double amount;
	String userName;
	String userId;
	double userBalance;
	char choice = '\0';
//	Store user details into database when database is implemented
	public TransactionService(String customerName, String inputID, String inputBalance)
	{
		userName = customerName;
		userId = inputID;
		userBalance = Double.parseDouble(inputBalance);
	}
	
//	@Test
	@SuppressWarnings("unused")
	public void activateUser(String uID, double num) 
	{
		LoginItemDaoImpl add5 = new LoginItemDaoImpl();
		double returnStatus = Double.parseDouble(add5.getAccountBalance(uID));
	}
	
	
//	@Test
	@SuppressWarnings("unused")
	public void deactivateUser(String uID, double num) 
	{
		LoginItemDaoImpl add6 = new LoginItemDaoImpl();
		double returnStatus = Double.parseDouble(add6.getAccountBalance(uID));
	}
	
	
//	@Test
	@SuppressWarnings("unused")
	public void withdraw(String uID, double num) 
	{
		LoginItemDaoImpl add3 = new LoginItemDaoImpl();
		double returnedBalance = Double.parseDouble(add3.getAccountBalance(uID));
		balance = returnedBalance - num;		
//		myFile1.writeFile("Withdrawal of $" + String.format("%.2f", num));
		LoginItemDaoImpl add11 = new LoginItemDaoImpl();
		String returnStatus = add11.depositBalance(uID,balance);
		System.out.println("$" + String.format("%.2f", num) + " withdrawn.");
	}
//	@Test
	@SuppressWarnings("unused")
	public void deposit(String uID, double num) 
	{
		LoginItemDaoImpl add1 = new LoginItemDaoImpl();
		double returnedBalance = Double.parseDouble(add1.getAccountBalance(uID));
		balance = returnedBalance + num;
//		myFile1.writeFile("Deposit of $" + String.format("%.2f", num));
		LoginItemDaoImpl add11 = new LoginItemDaoImpl();
		String returnStatus = add11.depositBalance(uID,balance);
		System.out.println("$" + String.format("%.2f", num) + " desposited.");
	}
//	@Test
	public void transaction(String inputID) 
	{
//		Take input from console

		System.out.println("Welcome back, " + userName + " (" + userId + ").");
		int compareChoice;
		Scanner userInput = new Scanner(System.in);
		do {
			
			LoginItemDaoImpl add2 = new LoginItemDaoImpl();
			double returnedBalance = Double.parseDouble(add2.getAccountBalance(inputID));
			System.out.println("Your current balance is $" + String.format("%.2f", returnedBalance) + ". ");
			System.out.println("");
			System.out.println("A) Make a deposit");
			System.out.println("B) Make a withdrawal");
			System.out.println("X) Previous Menu");
			System.out.println("");
			char choice = userInput.next().toUpperCase().charAt(0);
			switch(choice) 
			{
			case 'A':
				System.out.println("How much would you like to deposit?");
				amount = userInput.nextDouble();
				if (amount <= 0) 
				{
					System.out.println("Invalid amount. Please try again.");
				}
				else 
				{
					deposit(inputID, amount);
				}
				break;
			case 'B':
				System.out.println("How much would you like to withdraw?");
				amount = userInput.nextDouble();
				if (returnedBalance < amount) 
				{
					System.out.println("Insufficient Funds. Cannot perform this transaction.");
				}
				else if (amount <=0) 
				{
					System.out.println("Invalid amount. Please try again.");
				}
				else 
				{
					withdraw(inputID, amount);
				}
				break;
			case 'X':
				System.out.println("Have a nice day! Goodbye.");
				break;
			default:
				System.out.println("Invalid option. Please try again.");
				break;
			}
//			System.out.println(choice);
			compareChoice = Character.compare(choice, 'X');
		}
//		Not leaving the while loop. Test the condition.
		while(compareChoice != 0);
//		userInput.close();
		
	}
}
