package dev.ant.services;
import java.util.Scanner;
import dev.ant.daos.BankDaos;
import dev.ant.daos.LoginItemDaoImpl;


public class EmployeeService {
	
	String userName;
	String userId;
	char choice = '\0';
	
	public EmployeeService(String customerName, String inputID)
	{
		userName = customerName;
		userId = inputID;
	}

	@SuppressWarnings("unused")
	public void administration(String inputID) {
		Scanner userInput = new Scanner(System.in);	
		System.out.println("Welcome back, " + userName + " (" + userId + ").");
		int compareChoice;
		do {

			System.out.println("");
			System.out.println("A) View Account");
			System.out.println("B) Activate Account");
			System.out.println("C) DeActivate Account");
			System.out.println("X) Previous Menu");
			System.out.println("");
			char choice = userInput.next().toUpperCase().charAt(0);
			switch(choice) 
			{
			case 'A':
				String loginType="View Account";

				System.out.print("You selected " + loginType + "\nEnter UserID : ");
				inputID = userInput.next();
				LoginItemDaoImpl auth31 = new LoginItemDaoImpl();
				String returnedID = auth31.getUserId(inputID);
		
				LoginItemDaoImpl auth33 = new LoginItemDaoImpl();
				String returnedName = auth33.getAccountName(returnedID);
					
				LoginItemDaoImpl auth34 = new LoginItemDaoImpl();
				String returnedBalance = auth34.getAccountBalance(returnedID);
				
				LoginItemDaoImpl auth35 = new LoginItemDaoImpl();
				String returnedStatus = auth35.getUserStatus(returnedID);
				
				System.out.println("Welcome back, " + returnedName + " (" + returnedID + "-" + returnedStatus + ").");
				System.out.println("Your current balance is $" + String.format("%.2f", Double.parseDouble(returnedBalance)) + ". ");
								
				break;
			case 'B':
				System.out.print("\nEnter UserID : ");
				inputID = userInput.next();
				System.out.println("Activate Account" + inputID);
				LoginItemDaoImpl auth36 = new LoginItemDaoImpl();
				String returnedID5 = auth36.activateUser(inputID);
				break;
			case 'C':
				System.out.print("\nEnter UserID : ");
				inputID = userInput.next();
				System.out.println("Deactivate Account" + inputID);
				LoginItemDaoImpl auth37 = new LoginItemDaoImpl();
				String returnedID6 = auth37.deactivateUser(inputID);
				break;
			case 'X':
				System.out.println("Have a nice day! Goodbye.");
				break;
			default:
				System.out.println("Invalid option. Please try again.");
				break;
			}
			userInput.close();
//			System.out.println(choice);
			compareChoice = Character.compare(choice, 'X');
		}
//		Not leaving the while loop. Test the condition.
		while(compareChoice != 0);
//		userInput.close();
	}
	
}
