package dev.ant.daos;

public interface LoginDao {
	public boolean register(String email, String username, String password);
	public boolean signIn(String email, String password);
}
