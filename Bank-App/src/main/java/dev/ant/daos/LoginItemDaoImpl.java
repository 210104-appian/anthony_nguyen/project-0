package dev.ant.daos;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.ant.models.Customer;
import dev.ant.models.LoginItem;
import dev.ant.util.ConnectionUtil;


public class LoginItemDaoImpl{
	
//	private static Logger log = Logger.getRootLogger();
	
	public String getUserIdCount(String uName) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER where User_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("User_id"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = String.valueOf(types.size());
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String createUser (String uID, String uName, String uPW) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "INSERT INTO CUSTOMER VALUES (" + uID + ",'"+ uName + "',0,'PENDING','" + uName+uID + "',"+ uPW + ",'CUSTOMER')";

		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			statement.executeUpdate(query); // because we don't have any parameters, a normal statement is safe here
			con.close();
			return null;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 

		return null;
	}
	
	public String activateUser (String uID) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "UPDATE CUSTOMER SET STATUS = 'ACTIVE' Where User_Id = " + uID;

		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			statement.executeUpdate(query); // because we don't have any parameters, a normal statement is safe here
			con.close();
			return null;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 

		return null;
	}
	
	public String deactivateUser (String uID) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "UPDATE CUSTOMER SET STATUS = 'PENDING' Where User_Id = " + uID;

		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			statement.executeUpdate(query); // because we don't have any parameters, a normal statement is safe here
			con.close();
			return null;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 

		return null;
	}
	
	public String depositBalance (String uID, Double newBalance) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "UPDATE CUSTOMER SET BALANCE = " + newBalance + "Where User_Id = " + uID;

		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			statement.executeUpdate(query); // because we don't have any parameters, a normal statement is safe here
			con.close();
			return null;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 

		return null;
	}
	
	
	public String getUserId(String uName) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER where User_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("User_id"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			con.close();
			String returnvalue = types.get(0);
			if (types.size() == 1)
				return returnvalue;
			else
				return "0";
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getEmpId(String uName) {
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM EMPLOYEE where Emp_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Emp_id"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			con.close();
			String returnvalue = types.get(0);
			if (types.size() == 1)
				return returnvalue;
			else
				return "0";
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getPWFromUserID(String uName) {

		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER where User_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Password"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = types.get(0);
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getPWFromEmpID(String uName) {

		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM EMPLOYEE where Emp_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Password"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = types.get(0);
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getAccountName(String uName) {

		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER where User_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Name"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = types.get(0);
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getEmpName(String uName) {

		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM EMPLOYEE where Emp_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Name"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = types.get(0);
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getAccountBalance(String uName) {

		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER where User_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Balance"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = types.get(0);
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
	public String getUserStatus(String uName) {

		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER where User_id = " + uName;
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("Status"));
			}
//			System.out.println(types.get(0));
//			System.out.println("got "+types.size()+ " from db");
			String returnvalue = types.get(0);
			con.close();
			return returnvalue;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}
	
// ------------------------------------------------------------------------------------------
	
//	@Override
	public List<LoginItem> getAllLoginItems() {
		System.out.println("getting all Customer items");
		List<String> types = new ArrayList<>();
		String url = "jdbc:oracle:thin:@//antproject0.c59injbeavwf.us-east-2.rds.amazonaws.com:1521/ORCL";
		String LoginName = "admin";
		String password = "ap0Password";
		String query = "Select * FROM CUSTOMER";
		try {

			Connection con = DriverManager.getConnection(url, LoginName, password);
			Statement statement = con.createStatement();
			System.out.println("abc");
			ResultSet resultSet = statement.executeQuery(query); // because we don't have any parameters, a normal statement is safe here
			while(resultSet.next()) {
				types.add(resultSet.getString("LoginName"));
			}
			System.out.println(types);
			System.out.println("got "+types.size()+ " from db");
			
//			ResultSet resultSet2 = statement.executeQuery(query);
//			List<LoginItem> items = processLoginItemResultSet(resultSet2);
//			System.out.println(items);
//			System.out.println("got "+items.size()+ " from db");
//			return items;
			
		} catch (SQLException e) {
			System.out.println("Exception was thrown: " + e.getClass());
			e.printStackTrace();
		} 
		return null;
	}


//	@Override
	public LoginItem addNewLoginItem(LoginItem item) {
		// TODO Auto-generated method stub
		return null;
	}

	
	private List<LoginItem> processLoginItemResultSet(ResultSet resultSet) throws SQLException{
		List<LoginItem> items = new ArrayList<>();
		while(resultSet.next()) {
			String itemType = resultSet.getString("Status");
			switch(itemType) {
			case "ACTIVE":
				Customer activeCustomer = new Customer();
				activeCustomer.setId(resultSet.getInt("USER_ID"));
				activeCustomer.setName(resultSet.getString("Name"));
				activeCustomer.setName(resultSet.getString("LoginName"));
				activeCustomer.setPassword(resultSet.getString("Password"));
				System.out.println("oh no");
				break;
			case "PENDING":
				System.out.println("oh no");
				break;
			}
			
		}
			return items;
	}
}
